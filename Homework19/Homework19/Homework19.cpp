#include <iostream>
#include <clocale>
#include "Animals.h"

int main()
{
	setlocale(LC_ALL, "Russian");

	Animal* animal = new Animal;
	Animal* dog = new Dog;
	Animal* cat = new Cat;
	Animal* cow = new Cow;
	Animal* bird = new Bird;

	Animal* voices[5]{ animal, dog, cat, cow, bird };

	/*for (int i = 0; i < 5; i++)
		voices[i]->Voice();*/

	for (const auto& element : voices)         
	{
		element->Voice();
	}

	/*dog.Voice();
	cat.Voice();
	cow.Voice();
	bird.Voice();*/



}