#pragma once
#include <iostream>
class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "AnimalVoice\n";
	}
};

class Dog : public Animal
{
public:
	Dog() {};
	void Voice() override
	{
		std::cout << "��� ���\n";
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "��� ���\n";
	}
};

class Cow : public Animal
{
public:
	void Voice() override
	{
		std::cout << "�� ��\n";
	}
};

class Bird : public Animal
{
public:
	void Voice() override
	{
		std::cout << "��� �����\n";
	}
};